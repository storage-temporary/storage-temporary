<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--

 This file is part of Magatzem Temporal

 Copyright (C) 2006 Simó Albert i Beltran
 
 Magatzem Temporal is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published
 by the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Magatzem Temporal is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with Magatzem Temporal.  If not, see <http://www.gnu.org/licenses/>.

 http://www.gnu.org/licenses/agpl.txt

 Version 0.0.1

-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" >
	<head>
		<title>Magatzem temporal</title>
		<style type="text/css">
			body
			{
				text-align: center;
			}
			input
			{
				/*
				border: thin solid black;
				*/
				margin: 0.5em;
			}
			.error
			{
				color: red;
			}

		</style>
	</head>
	<body style="text-align:center;">
		<h1>Magatzem temporal</h1>
		<?php echo $output; ?>
		<p><a style="font-size:x-small;" href="http://probeta.net/dokuwiki/doku.php?id=guies_dels_serveis:magatzem_temporal">qu&egrave; &eacute;s aix&ograve;?</a></p>
		<div style='opacity:0.5; font-size: x-small'>
			<div>
				Powered by <a href="https://gitlab.com/sim6/storage-temporary">storage-temporary</a>
			</div>
			<div>
				<a href="http://www.gnu.org/licenses/agpl-3.0.html"><img src="http://www.gnu.org/graphics/agplv3-155x51.png" alt="AGPLv3" style='border:none; height:25px;'/></a>
				<a href='http://validator.w3.org/check?uri=referer'><img src='http://www.w3.org/Icons/valid-xhtml10-blue' alt='Valid XHTML 1.0 Strict' style='border:none; height:25px;' /></a>
				<a href='http://jigsaw.w3.org/css-validator/check/referer?profile=css3'><img alt='Valid CSS!' src='http://www.w3.org/Icons/valid-css-blue.png' style='border:none ; height:25px;'/></a>
				<a href='http://www.w3.org/WAI/WCAG1A-Conformance'><img alt='Nivel A de las Directrices de Accesibilidad para el Contenido Web 1.0 del W3C-WAI' src='http://www.w3.org/WAI/wcag1A-blue' style='border:none ; height:25px'/></a>
			</div>
		</div>
	</body>
</html>